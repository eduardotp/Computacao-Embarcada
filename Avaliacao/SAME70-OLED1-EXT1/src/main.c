/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
 #include <asf.h>
 
 #include "oled/gfx_mono_ug_2832hsweg04.h"
 #include "oled/gfx_mono_text.h"
 #include "oled/sysfont.h"
 
 #define BUT2_PIO_ID			  ID_PIOC
 #define BUT2_PIO				  PIOC
 #define BUT2_PIN				  31
 #define BUT2_PIN_MASK			  (1 << BUT2_PIN)
 #define BUT2_DEBOUNCING_VALUE  79
 
 #define BUT3_PIO_ID			  ID_PIOA
 #define BUT3_PIO				  PIOA
 #define BUT3_PIN				  19
 #define BUT3_PIN_MASK			  (1 << BUT3_PIN)
 #define BUT3_DEBOUNCING_VALUE  79
 
 #define LED2_PIO_ID	   ID_PIOC
 #define LED2_PIO        PIOC
 #define LED2_PIN		   30
 #define LED2_PIN_MASK   (1<<LED2_PIN)
 
 #define LED1_PIO_ID	   ID_PIOA
 #define LED1_PIO        PIOA
 #define LED1_PIN		   0
 #define LED1_PIN_MASK   (1<<LED1_PIN)
 
 #define YEAR        2018
 #define MOUNTH      4
 #define DAY         9
 #define WEEK        15
 #define HOUR        13
 #define MINUTE      5
 #define SECOND      0
 
 volatile bool alarm_exists;
 volatile bool flag_alarm;
 volatile bool flag_led2;
 volatile uint8_t alarm_length;

 
 volatile int hour;
 volatile int minute;
 volatile int second;
 
 
 /************************************************************************/
 /* PROTOTYPES                                                           */
 /************************************************************************/
 
 void BUT_init(void);
 void LED_init(int estado);
 void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
 void RTC_init(void);
 void pin_toggle(Pio *pio, uint32_t mask);
 
 void TC1_Handler(void){
     volatile uint32_t ul_dummy;
 
     ul_dummy = tc_get_status(TC0, 1);
 
     /* Avoid compiler warning */
     UNUSED(ul_dummy);
 
     /** Muda o estado do LED */
     if(flag_led2){
         pin_toggle(LED2_PIO, LED2_PIN_MASK);
     }
     flag_led2 = !flag_led2;
 }
 
 static void Button2_Handler(uint32_t id, uint32_t mask){
	 alarm_exists = true;
     alarm_length +=1;
	 pio_clear(LED1_PIO, LED1_PIN_MASK);
     rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
     rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
     rtc_clear_status(RTC, RTC_SCCR_CALCLR);
     rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	 rtc_get_time(RTC,&hour,&minute,&second);
	 char horario[150];
	 if(minute>=10){
		 sprintf(horario, "%d:%d-%d", hour, minute, alarm_length);
	 }
	 else{
		 sprintf(horario, "%d:0%d-%d", hour, minute, alarm_length);
	 }
	 gfx_mono_draw_string(horario, 0, 0, &sysfont);
     rtc_set_time_alarm(RTC, 1, hour, 1, minute+alarm_length, 1, second);
 }
 
 static void Button3_Handler(uint32_t id, uint32_t mask){
     alarm_exists = false;
     pio_set(LED1_PIO, LED1_PIN_MASK);
	 alarm_length = 0;
     rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
     rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
     rtc_clear_status(RTC, RTC_SCCR_CALCLR);
     rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	 char horario[150];
	 if(minute>=10){
		 sprintf(horario, "%d:%d-%d", hour, minute, alarm_length);
	 }
	 else{
		 sprintf(horario, "%d:0%d-%d", hour, minute, alarm_length);
	 }
	 gfx_mono_draw_string(horario, 0, 0, &sysfont);
	 pmc_disable_periph_clk(ID_TC1);
 }
 
 void pin_toggle(Pio *pio, uint32_t mask){
     if(pio_get_output_data_status(pio, mask))
     pio_clear(pio, mask);
     else
     pio_set(pio,mask);
 }
 
 
 void RTC_Handler(void)
 {
     uint32_t ul_status = rtc_get_status(RTC);
 
     /*
     *  Verifica por qual motivo entrou
     *  na interrupcao, se foi por segundo
     *  ou Alarm
     */
     if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
         rtc_clear_status(RTC, RTC_SCCR_SECCLR);
         rtc_get_time(RTC, &hour, &minute, &second);
         
         if(second == 0){
             char horario[150];
			 if(minute>=10){
				sprintf(horario, "%d:%d-%d", hour, minute, alarm_length);
			 }
			 else{
				 sprintf(horario, "%d:0%d-%d", hour, minute, alarm_length);
			 }
			 gfx_mono_draw_string(horario, 0, 0, &sysfont);
         }
     }
     
     /* Time or date alarm */
     if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
         TC_init(TC0, ID_TC1, 1, 8);
         rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
         pio_set(LED1_PIO, LED1_PIN_MASK);
         
     }
     
     rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
     rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
     rtc_clear_status(RTC, RTC_SCCR_CALCLR);
     rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
     
 }
 
 void LED_init(int estado){
     
     pmc_enable_periph_clk(LED1_PIO_ID);
     pio_set_output(LED1_PIO, LED1_PIN_MASK, estado, 0, 0 );
     
     pmc_enable_periph_clk(LED2_PIO_ID);
     pio_set_output(LED2_PIO, LED2_PIN_MASK, estado, 0, 0 );
     
 };
 
 void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
     uint32_t ul_div;
     uint32_t ul_tcclks;
     uint32_t ul_sysclk = sysclk_get_cpu_hz();
 
     uint32_t channel = 1;
 
     pmc_enable_periph_clk(ID_TC);
 
     /** Configura o TC para operar em  4Mhz e interrupco no RC compare */
     tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
     tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
     tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);
 
     /* Configura e ativa interrupco no TC canal 0 */
     /* Interrupo no C */
     NVIC_EnableIRQ((IRQn_Type) ID_TC);
     tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS); //INTERRUPCAO
 
     /* Inicializa o canal 0 do TC */
     tc_start(TC, TC_CHANNEL);
 }
 
 void BUT_init(void){
     //BOTAO 2
     
     /* config. pino botao em modo de entrada */
     pmc_enable_periph_clk(BUT2_PIO_ID);
     pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
 
     /* config. interrupcao em borda de descida no botao do kit */
     /* indica funcao (but_Handler) a ser chamada quando houver uma interrupo */
     pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);// INTERRUPCAO
     pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, Button2_Handler);
 
     /* habilita interrupco do PIO que controla o botao */
     /* e configura sua prioridade                        */
     NVIC_EnableIRQ(BUT2_PIO_ID);
     NVIC_SetPriority(BUT2_PIO_ID, 1);
     
     //BOTAO 3
     
     /* config. pino botao em modo de entrada */
     pmc_enable_periph_clk(BUT3_PIO_ID);
     pio_set_input(BUT3_PIO, BUT3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
 
     /* config. interrupcao em borda de descida no botao do kit */
     /* indica funcao (but_Handler) a ser chamada quando houver uma interrupo */
     pio_enable_interrupt(BUT3_PIO, BUT3_PIN_MASK);// INTERRUPCAO
     pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, PIO_IT_FALL_EDGE, Button3_Handler);
 
     /* habilita interrupco do PIO que controla o botao */
     /* e configura sua prioridade                        */
     NVIC_EnableIRQ(BUT3_PIO_ID);
     NVIC_SetPriority(BUT3_PIO_ID, 1);
 };
 
 void RTC_init(){
     /* Configura o PMC */
     pmc_enable_periph_clk(ID_RTC);
     /* Default RTC configuration, 24-hour mode */
     rtc_set_hour_mode(RTC, 0);
     /* Configura data e hora manualmente */
     rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
     rtc_set_time(RTC, HOUR, MINUTE, SECOND);
     /* Configure RTC interrupts */
     NVIC_DisableIRQ(RTC_IRQn);
     NVIC_ClearPendingIRQ(RTC_IRQn);
     NVIC_SetPriority(RTC_IRQn, 0);
     NVIC_EnableIRQ(RTC_IRQn);
     /* Ativa interrupcao via alarme */
     rtc_enable_interrupt(RTC,  RTC_IER_ALREN | RTC_IER_SECEN); //INTERRUPCAO
 }
 
 int main (void)
 {
     board_init();
     sysclk_init();
     delay_init();
     WDT->WDT_MR = WDT_MR_WDDIS;
     gfx_mono_ssd1306_init();
     
     BUT_init();
     LED_init(0);
 
     /** Configura RTC */
     RTC_init();
 
     /* configura alarme do RTC */
     //rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
     
     
     
     //char horario[150];
     //sprintf(horario, "%d:%d:%d", hour, minute, second);
     //gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
     //gfx_mono_draw_string(s, 0, 0, &sysfont);
     
     while(1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
     }
 }
 